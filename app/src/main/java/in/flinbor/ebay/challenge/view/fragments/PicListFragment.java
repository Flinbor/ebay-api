/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.ebay.challenge.view.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.flinbor.ebay.challenge.R;
import in.flinbor.ebay.challenge.presenter.PicturesListPresenter;
import in.flinbor.ebay.challenge.presenter.vo.PictureVO;
import in.flinbor.ebay.challenge.view.adapter.PicListAdapter;


/**
 * Implementation of view layer, responsible for UI
 */
public class PicListFragment extends Fragment implements PicturesListView {
    @BindView(R.id.recycler_view)
    protected RecyclerView recyclerView;
    private PicListAdapter adapter;
    private PicturesListPresenter presenter;

    public static Fragment getInstance() {
        return new PicListFragment();
    }

    @Override
    public void setPresenter(@NonNull PicturesListPresenter presenter) {
        this.presenter = presenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_pic_list, container, false);
        ButterKnife.bind(this, view);
        // Set Fragment title
        initRecyclerVeiw();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Notify presenter
        presenter.onViewCreated(savedInstanceState);
    }

    private void initRecyclerVeiw() {
        // Initialization of recycler view
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);
        adapter = new PicListAdapter(this, presenter);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showError(@NonNull String error) {
        makeToast(error);
    }


    @Override
    public void showEmptyList() {
        adapter.clear();
    }

    @Override
    public void showPictures(@NonNull List<PictureVO> picList) {
        adapter.setList(picList);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (presenter != null) {
            presenter.onSaveInstanceState(outState);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (presenter != null) {
            presenter.onStop();
        }
    }

    /**
     * show short message to user
     * @param text message to show
     */
    private void makeToast(String text) {
        Snackbar.make(recyclerView, text, Snackbar.LENGTH_LONG).show();
    }

}
