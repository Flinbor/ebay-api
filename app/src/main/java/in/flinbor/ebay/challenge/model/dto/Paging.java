
package in.flinbor.ebay.challenge.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Paging {

    @SerializedName("numFound")
    @Expose
    private String numFound;
    @SerializedName("link")
    @Expose
    private List<Link_> link = new ArrayList<Link_>();

    /**
     * 
     * @return
     *     The numFound
     */
    public String getNumFound() {
        return numFound;
    }

    /**
     * 
     * @param numFound
     *     The numFound
     */
    public void setNumFound(String numFound) {
        this.numFound = numFound;
    }

    /**
     * 
     * @return
     *     The link
     */
    public List<Link_> getLink() {
        return link;
    }

    /**
     * 
     * @param link
     *     The link
     */
    public void setLink(List<Link_> link) {
        this.link = link;
    }

}
