
package in.flinbor.ebay.challenge.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Pictures_ {

    @SerializedName("picture")
    @Expose
    private List<Picture> picture = new ArrayList<Picture>();

    /**
     * 
     * @return
     *     The picture
     */
    public List<Picture> getPicture() {
        return picture;
    }

    /**
     * 
     * @param picture
     *     The picture
     */
    public void setPicture(List<Picture> picture) {
        this.picture = picture;
    }

}
