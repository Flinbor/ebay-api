
package in.flinbor.ebay.challenge.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ad {

    @SerializedName("pictures")
    @Expose
    private Pictures_ pictures;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("otherAttributes")
    @Expose
    private OtherAttributes otherAttributes;

    /**
     * 
     * @return
     *     The pictures
     */
    public Pictures_ getPictures() {
        return pictures;
    }

    /**
     * 
     * @param pictures
     *     The pictures
     */
    public void setPictures(Pictures_ pictures) {
        this.pictures = pictures;
    }

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The otherAttributes
     */
    public OtherAttributes getOtherAttributes() {
        return otherAttributes;
    }

    /**
     * 
     * @param otherAttributes
     *     The otherAttributes
     */
    public void setOtherAttributes(OtherAttributes otherAttributes) {
        this.otherAttributes = otherAttributes;
    }

}
