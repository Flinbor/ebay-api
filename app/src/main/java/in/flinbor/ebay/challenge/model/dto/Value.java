
package in.flinbor.ebay.challenge.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Value {

    @SerializedName("ad")
    @Expose
    private List<Ad> ad = new ArrayList<Ad>();
    @SerializedName("paging")
    @Expose
    private Paging paging;
    @SerializedName("ads-search-options")
    @Expose
    private AdsSearchOptions adsSearchOptions;
    @SerializedName("ads-search-histograms")
    @Expose
    private AdsSearchHistograms adsSearchHistograms;
    @SerializedName("ads-search-result-metadata")
    @Expose
    private AdsSearchResultMetadata adsSearchResultMetadata;
    @SerializedName("ads-search-keyword-refine-suggestions")
    @Expose
    private AdsSearchKeywordRefineSuggestions adsSearchKeywordRefineSuggestions;

    /**
     * 
     * @return
     *     The ad
     */
    public List<Ad> getAd() {
        return ad;
    }

    /**
     * 
     * @param ad
     *     The ad
     */
    public void setAd(List<Ad> ad) {
        this.ad = ad;
    }

    /**
     * 
     * @return
     *     The paging
     */
    public Paging getPaging() {
        return paging;
    }

    /**
     * 
     * @param paging
     *     The paging
     */
    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    /**
     * 
     * @return
     *     The adsSearchOptions
     */
    public AdsSearchOptions getAdsSearchOptions() {
        return adsSearchOptions;
    }

    /**
     * 
     * @param adsSearchOptions
     *     The ads-search-options
     */
    public void setAdsSearchOptions(AdsSearchOptions adsSearchOptions) {
        this.adsSearchOptions = adsSearchOptions;
    }

    /**
     * 
     * @return
     *     The adsSearchHistograms
     */
    public AdsSearchHistograms getAdsSearchHistograms() {
        return adsSearchHistograms;
    }

    /**
     * 
     * @param adsSearchHistograms
     *     The ads-search-histograms
     */
    public void setAdsSearchHistograms(AdsSearchHistograms adsSearchHistograms) {
        this.adsSearchHistograms = adsSearchHistograms;
    }

    /**
     * 
     * @return
     *     The adsSearchResultMetadata
     */
    public AdsSearchResultMetadata getAdsSearchResultMetadata() {
        return adsSearchResultMetadata;
    }

    /**
     * 
     * @param adsSearchResultMetadata
     *     The ads-search-result-metadata
     */
    public void setAdsSearchResultMetadata(AdsSearchResultMetadata adsSearchResultMetadata) {
        this.adsSearchResultMetadata = adsSearchResultMetadata;
    }

    /**
     * 
     * @return
     *     The adsSearchKeywordRefineSuggestions
     */
    public AdsSearchKeywordRefineSuggestions getAdsSearchKeywordRefineSuggestions() {
        return adsSearchKeywordRefineSuggestions;
    }

    /**
     * 
     * @param adsSearchKeywordRefineSuggestions
     *     The ads-search-keyword-refine-suggestions
     */
    public void setAdsSearchKeywordRefineSuggestions(AdsSearchKeywordRefineSuggestions adsSearchKeywordRefineSuggestions) {
        this.adsSearchKeywordRefineSuggestions = adsSearchKeywordRefineSuggestions;
    }

}
