/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.ebay.challenge.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.flinbor.ebay.challenge.R;
import in.flinbor.ebay.challenge.app.App;
import in.flinbor.ebay.challenge.model.Model;
import in.flinbor.ebay.challenge.presenter.PicListPresenterImpl;
import in.flinbor.ebay.challenge.presenter.PicturesListPresenter;
import in.flinbor.ebay.challenge.presenter.mapper.PictureMapper;
import in.flinbor.ebay.challenge.view.fragments.PicListFragment;
import in.flinbor.ebay.challenge.view.fragments.PicturesListView;



/**
 * The activity that is loaded when the application starts
 * controls fragments creation
 */
public class MainActivity extends AppCompatActivity {
    private FragmentManager fragmentManager;

    @BindView(R.id.activity_maion_toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        fragmentManager = getSupportFragmentManager();
        if (savedInstanceState == null) {
            Fragment fragment = PicListFragment.getInstance();
            replaceFragment(fragment, false);
            injectFragments(fragment);
        }
    }


    //todo: replace with Dagger injection
    private void injectFragments(Fragment fragment) {
        if (fragment instanceof PicturesListView) {
            //inject mapper for junit test
            PictureMapper mapper = new PictureMapper();
            bindRepoListMVP((PicturesListView) fragment, new PicListPresenterImpl(mapper), App.getApp().getModelInstance());
        }
    }

    /**
     * Creating links between model-view-presenter
     */
    private void bindRepoListMVP(@NonNull PicturesListView view, @NonNull PicturesListPresenter presenter, @NonNull Model model) {
        view.setPresenter(presenter);
        presenter.setView(view);
        presenter.setModel(model);
    }


    /**
     * Set fragment to default activity container
     *
     * @param fragment     Fragment to insert
     * @param addBackStack If true -> fragment will be added to backStack
     */
    private void replaceFragment(final Fragment fragment, boolean addBackStack) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.activity_main_frame_layout, fragment, fragment.getClass().getSimpleName());
        if (addBackStack) transaction.addToBackStack(null);
        transaction.commit();
    }

}
