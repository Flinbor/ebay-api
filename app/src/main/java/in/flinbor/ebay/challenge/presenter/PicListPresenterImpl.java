/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.ebay.challenge.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import in.flinbor.ebay.challenge.app.App;
import in.flinbor.ebay.challenge.model.dto.PicturesDTO;
import in.flinbor.ebay.challenge.presenter.mapper.PictureMapper;
import in.flinbor.ebay.challenge.presenter.vo.PictureVO;
import in.flinbor.ebay.challenge.view.fragments.PicturesListView;
import rx.Observable;
import rx.Observer;
import rx.Subscription;


/**
 * Concrete implementation of Presenter
 * responsible for Pictures-Screen related business logic
 */
public class PicListPresenterImpl extends BasePresenter implements PicturesListPresenter {
    public static final String TAG = PicListPresenterImpl.class.getSimpleName();
    private final PictureMapper repoMapper;
    private PicturesListView view;
    private Subscription subscription;

    public PicListPresenterImpl(PictureMapper mapper) {
        repoMapper = mapper;
    }

    @Override
    public void onViewCreated(Bundle savedInstanceState) {
        loadReports();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    @Override
    public void setView(@NonNull PicturesListView view) {
        this.view = view;
    }

    @Override
    public void onItemSelected(PictureVO item) {
        Toast.makeText(App.getApp(), "show detail", Toast.LENGTH_SHORT).show();
    }



    private void loadReports() {
        if (subscription != null) {
            // Retrofit RxJava connector will redirect this call to okHttp's cancel
            subscription.unsubscribe();
        }
        Observable<PicturesDTO> repoList = getModel().getPictures();
        subscription = repoList
                .map(repoMapper)
                .subscribe(new Observer<List<PictureVO>>() {

                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError");
                        view.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(List<PictureVO> list) {
                        Log.d(TAG, "onNext");
                        if (list == null || list.size() == 0) {
                            Log.d(TAG, "list is null or empty");
                            view.showEmptyList();
                        } else {
                            Log.d(TAG, "list with data received");
                            view.showPictures(list);
                        }
                    }
                });

        addSubscription(subscription);
    }
}
