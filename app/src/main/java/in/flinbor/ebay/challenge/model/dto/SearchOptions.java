
package in.flinbor.ebay.challenge.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchOptions {

    @SerializedName("extension")
    @Expose
    private Extension extension;
    @SerializedName("attr")
    @Expose
    private Attr attr;
    @SerializedName("pictureRequired")
    @Expose
    private Boolean pictureRequired;
    @SerializedName("includeTopAds")
    @Expose
    private Boolean includeTopAds;

    /**
     * 
     * @return
     *     The extension
     */
    public Extension getExtension() {
        return extension;
    }

    /**
     * 
     * @param extension
     *     The extension
     */
    public void setExtension(Extension extension) {
        this.extension = extension;
    }

    /**
     * 
     * @return
     *     The attr
     */
    public Attr getAttr() {
        return attr;
    }

    /**
     * 
     * @param attr
     *     The attr
     */
    public void setAttr(Attr attr) {
        this.attr = attr;
    }

    /**
     * 
     * @return
     *     The pictureRequired
     */
    public Boolean getPictureRequired() {
        return pictureRequired;
    }

    /**
     * 
     * @param pictureRequired
     *     The pictureRequired
     */
    public void setPictureRequired(Boolean pictureRequired) {
        this.pictureRequired = pictureRequired;
    }

    /**
     * 
     * @return
     *     The includeTopAds
     */
    public Boolean getIncludeTopAds() {
        return includeTopAds;
    }

    /**
     * 
     * @param includeTopAds
     *     The includeTopAds
     */
    public void setIncludeTopAds(Boolean includeTopAds) {
        this.includeTopAds = includeTopAds;
    }

}
