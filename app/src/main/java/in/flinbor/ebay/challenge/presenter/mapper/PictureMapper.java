/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.ebay.challenge.presenter.mapper;

import android.text.TextUtils;

import java.util.List;

import in.flinbor.ebay.challenge.model.dto.Ad;
import in.flinbor.ebay.challenge.model.dto.Link;
import in.flinbor.ebay.challenge.model.dto.Picture;
import in.flinbor.ebay.challenge.model.dto.PicturesDTO;
import in.flinbor.ebay.challenge.presenter.vo.PictureVO;
import rx.Observable;
import rx.functions.Func1;


/**
 * Class converter of Data Transfer Object to View Object
 * RepositoriesDTO -> RepoItem
 */
public class PictureMapper implements Func1<PicturesDTO, List<PictureVO>> {

    public static final String THUMBNAIL = "thumbnail";
    public static final String LARGE = "large";

    public List<PictureVO> call(PicturesDTO picturesDTO) {
        if (picturesDTO == null) {
            return null;
        }

        final List<Ad> adList = picturesDTO.getHttpWwwEbayclassifiedsgroupComSchemaAdV1Ads().getValue().getAd();

        return Observable.from(adList)
                .map(ad -> {
                    return getPictureVO(ad);
                })
                .toList()
                .toBlocking()
                .first();
    }

    /**
     * todo: add NULL check
     * @param ad
     * @return
     */
    private PictureVO getPictureVO(Ad ad) {
        PictureVO pictureVO = new PictureVO();
        pictureVO.setId(ad.getId());

        for (Picture p : ad.getPictures().getPicture()) {
            for (Link link : p.getLink()) {
                if (TextUtils.equals(link.getRel(), LARGE)) {
                    pictureVO.setUrlLarge(link.getHref());
                }
                if (TextUtils.equals(link.getRel(), THUMBNAIL)) {
                    pictureVO.setUrlThumb(link.getHref());
                }
            }
        }
        return pictureVO;
    }


}
