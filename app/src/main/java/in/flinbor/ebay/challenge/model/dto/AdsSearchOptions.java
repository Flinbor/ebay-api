
package in.flinbor.ebay.challenge.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AdsSearchOptions {

    @SerializedName("page")
    @Expose
    private Page page;
    @SerializedName("size")
    @Expose
    private Size size;
    @SerializedName("pictureRequired")
    @Expose
    private PictureRequired pictureRequired;
    @SerializedName("indexDate")
    @Expose
    private IndexDate indexDate;
    @SerializedName("includeTopAds")
    @Expose
    private IncludeTopAds includeTopAds;

    /**
     * 
     * @return
     *     The page
     */
    public Page getPage() {
        return page;
    }

    /**
     * 
     * @param page
     *     The page
     */
    public void setPage(Page page) {
        this.page = page;
    }

    /**
     * 
     * @return
     *     The size
     */
    public Size getSize() {
        return size;
    }

    /**
     * 
     * @param size
     *     The size
     */
    public void setSize(Size size) {
        this.size = size;
    }

    /**
     * 
     * @return
     *     The pictureRequired
     */
    public PictureRequired getPictureRequired() {
        return pictureRequired;
    }

    /**
     * 
     * @param pictureRequired
     *     The pictureRequired
     */
    public void setPictureRequired(PictureRequired pictureRequired) {
        this.pictureRequired = pictureRequired;
    }

    /**
     * 
     * @return
     *     The indexDate
     */
    public IndexDate getIndexDate() {
        return indexDate;
    }

    /**
     * 
     * @param indexDate
     *     The indexDate
     */
    public void setIndexDate(IndexDate indexDate) {
        this.indexDate = indexDate;
    }

    /**
     * 
     * @return
     *     The includeTopAds
     */
    public IncludeTopAds getIncludeTopAds() {
        return includeTopAds;
    }

    /**
     * 
     * @param includeTopAds
     *     The includeTopAds
     */
    public void setIncludeTopAds(IncludeTopAds includeTopAds) {
        this.includeTopAds = includeTopAds;
    }

}
