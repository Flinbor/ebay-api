
package in.flinbor.ebay.challenge.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PicturesDTO {

    @SerializedName("searchOptions")
    @Expose
    private SearchOptions searchOptions;
    @SerializedName("{http://www.ebayclassifiedsgroup.com/schema/ad/v1}ads")
    @Expose
    private HttpWwwEbayclassifiedsgroupComSchemaAdV1Ads httpWwwEbayclassifiedsgroupComSchemaAdV1Ads;

    /**
     * 
     * @return
     *     The searchOptions
     */
    public SearchOptions getSearchOptions() {
        return searchOptions;
    }

    /**
     * 
     * @param searchOptions
     *     The searchOptions
     */
    public void setSearchOptions(SearchOptions searchOptions) {
        this.searchOptions = searchOptions;
    }

    /**
     * 
     * @return
     *     The httpWwwEbayclassifiedsgroupComSchemaAdV1Ads
     */
    public HttpWwwEbayclassifiedsgroupComSchemaAdV1Ads getHttpWwwEbayclassifiedsgroupComSchemaAdV1Ads() {
        return httpWwwEbayclassifiedsgroupComSchemaAdV1Ads;
    }

    /**
     * 
     * @param httpWwwEbayclassifiedsgroupComSchemaAdV1Ads
     *     The {http://www.ebayclassifiedsgroup.com/schema/ad/v1}ads
     */
    public void setHttpWwwEbayclassifiedsgroupComSchemaAdV1Ads(HttpWwwEbayclassifiedsgroupComSchemaAdV1Ads httpWwwEbayclassifiedsgroupComSchemaAdV1Ads) {
        this.httpWwwEbayclassifiedsgroupComSchemaAdV1Ads = httpWwwEbayclassifiedsgroupComSchemaAdV1Ads;
    }

}
