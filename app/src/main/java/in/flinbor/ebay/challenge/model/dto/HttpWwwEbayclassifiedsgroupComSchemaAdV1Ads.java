
package in.flinbor.ebay.challenge.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HttpWwwEbayclassifiedsgroupComSchemaAdV1Ads {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("declaredType")
    @Expose
    private String declaredType;
    @SerializedName("scope")
    @Expose
    private String scope;
    @SerializedName("value")
    @Expose
    private Value value;
    @SerializedName("nil")
    @Expose
    private Boolean nil;
    @SerializedName("globalScope")
    @Expose
    private Boolean globalScope;
    @SerializedName("typeSubstituted")
    @Expose
    private Boolean typeSubstituted;

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The declaredType
     */
    public String getDeclaredType() {
        return declaredType;
    }

    /**
     * 
     * @param declaredType
     *     The declaredType
     */
    public void setDeclaredType(String declaredType) {
        this.declaredType = declaredType;
    }

    /**
     * 
     * @return
     *     The scope
     */
    public String getScope() {
        return scope;
    }

    /**
     * 
     * @param scope
     *     The scope
     */
    public void setScope(String scope) {
        this.scope = scope;
    }

    /**
     * 
     * @return
     *     The value
     */
    public Value getValue() {
        return value;
    }

    /**
     * 
     * @param value
     *     The value
     */
    public void setValue(Value value) {
        this.value = value;
    }

    /**
     * 
     * @return
     *     The nil
     */
    public Boolean getNil() {
        return nil;
    }

    /**
     * 
     * @param nil
     *     The nil
     */
    public void setNil(Boolean nil) {
        this.nil = nil;
    }

    /**
     * 
     * @return
     *     The globalScope
     */
    public Boolean getGlobalScope() {
        return globalScope;
    }

    /**
     * 
     * @param globalScope
     *     The globalScope
     */
    public void setGlobalScope(Boolean globalScope) {
        this.globalScope = globalScope;
    }

    /**
     * 
     * @return
     *     The typeSubstituted
     */
    public Boolean getTypeSubstituted() {
        return typeSubstituted;
    }

    /**
     * 
     * @param typeSubstituted
     *     The typeSubstituted
     */
    public void setTypeSubstituted(Boolean typeSubstituted) {
        this.typeSubstituted = typeSubstituted;
    }

}
