/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.ebay.challenge.model;

import android.support.annotation.NonNull;

import in.flinbor.ebay.challenge.api.ApiInterface;
import in.flinbor.ebay.challenge.model.dto.PicturesDTO;
import in.flinbor.ebay.constant.Constants;
import rx.Observable;
import rx.Scheduler;


/**
 * Implementation of Model
 * responsible for receiving
 */
public class ModelImpl implements Model {

    private final ApiInterface apiInterface;
    private final Scheduler     subscribeOn;
    private final Scheduler     observeOn;

    /**
     * Constructor with customizable parameters
     *
     * @param apiInterface Api interface or mock
     * @param subscribeOn Thread on which will be performed main calculation (for the JUnit should be mocked with main thread)
     * @param observeOn Thread to with will be dispatched result (for the JUnit should be mocked with main thread)
     */
    public ModelImpl(@NonNull ApiInterface apiInterface, @NonNull Scheduler subscribeOn, @NonNull Scheduler observeOn) {
        this.apiInterface = apiInterface;
        this.subscribeOn = subscribeOn;
        this.observeOn = observeOn;
    }

    @Override
    public Observable<PicturesDTO> getPictures() {
        String auth = Constants.USER + ":" + Constants.PASSWORD;
//        try {
//            final byte[] bytes = auth.getBytes("UTF-8");
//            String base64 = Base64.encodeToString(bytes, Base64.);

            return apiInterface
                    .getPictures(Constants.PATH_ADS_JSON, "Basic ZmFjZWJvb2s6dHpmczUzbWY=", Constants.QUERY_IMAGE)
                    .compose(applySchedulers());
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        return null;
    }

    private <T> Observable.Transformer<T, T> applySchedulers() {
        return observable -> observable.subscribeOn(subscribeOn)
                .observeOn(observeOn);
    }

}
