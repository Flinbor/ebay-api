/*
 * Copyright 2016 Flinbor Bogdanov Oleksandr
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package in.flinbor.ebay.constant;

/**
 * Global constants
 */
public interface Constants {
    String BASE_URL             = "https://api.ebay-kleinanzeigen.de/api/";
    String PATH_ADS_JSON        = "ads.json";
    String QUERY_IMAGE          = "pictures";
    String USER                 = "facebook";
    String PASSWORD             = "tzfs53mf";
}
